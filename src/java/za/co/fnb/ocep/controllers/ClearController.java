package za.co.fnb.ocep.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIClearController;
import za.co.fnb.pe.framework.api.UIControllerContext;

public class ClearController implements UIClearController {
	private static final Logger LOG = Logger.getLogger( ClearController.class );
	
	@Override
	public void processMessage(UIControllerContext context) {
				
//		This is an example of a class that can be used to clear a session, everytime the app is logged off or reset somewhow
//		this class must just be implemented using the a UIClearController, and specifed in the xml like below:
//		
//		<clearcontroller class="za.co.fnb.ocep.controllers.LoadTabsController" />
		
		LOG.info("Stuff to be clean up");
		
	}
}
