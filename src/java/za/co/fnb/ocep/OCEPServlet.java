package za.co.fnb.ocep;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import za.co.fnb.pe.framework.PlatformExtensionServlet;


public class OCEPServlet extends PlatformExtensionServlet implements Servlet {
	
	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		super.service(request, response);
		
	}
	
	protected String loadXMLFile(ServletConfig servletConfig)  {
		ServletContext context = servletConfig.getServletContext();
		String fullPath = context.getRealPath("/WEB-INF/classes/platform-extension-config.xml");
		return fullPath;		
	}

}
